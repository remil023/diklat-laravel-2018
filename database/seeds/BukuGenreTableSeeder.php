<?php

use Illuminate\Database\Seeder;

class BukuGenreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('buku_genres')->insert([[
            'genre' => 'Horror'
        ],[
            'genre' => 'Comic'
        ],[
            'genre' => 'IT'
        ],[
            'genre' => 'Novel'
        ]]);
    }
}
