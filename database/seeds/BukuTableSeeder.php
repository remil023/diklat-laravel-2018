<?php

use Illuminate\Database\Seeder;

class BukuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('buku')->insert([[
            'judul' => 'Laravel for noob',
            'author' => 'Baginda',
            'year' => 2018,
            'genre_id'  => 3
        ],[
            'judul' => 'Laravel Advance',
            'author' => 'Violet',
            'year' => 2018,
            'genre_id'  => 3
        ],[
            'judul' => 'Crayon Shincan',
            'author' => 'Pak Eko',
            'year' => 2018,
            'genre_id'  => 2
        ],[
            'judul' => 'Splinter Cell',
            'author' => 'CIA',
            'year' => 2018,
            'genre_id'  => 4
        ],[
            'judul' => 'One Piece',
            'author' => 'Bu Eko',
            'year' => 2018,
            'genre_id'  => 2
        ],[
            'judul' => 'Naruto',
            'author' => 'Nak Eko',
            'year' => 2018,
            'genre_id'  => 2
        ]]);
    }
}
