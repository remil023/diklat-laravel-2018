<?php

use Illuminate\Database\Seeder;

class BackpackSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([[
            'name' => 'Remil Hidayat',
            'email' => 'remil.hidayat@gmail.com',
            'password' => '$2y$10$UbDu4gOlJ/qgUzCvDSKheOfdxN6y3wTeoLeYGq6Qy30nhjcH5aMRm'
        ],[
            'name' => 'Pak Budi',
            'email' => 'budi@gmail.com',
            'password' => ''
        ],[
            'name' => 'Pak Eko',
            'email' => 'eko@gmail.com',
            'password' => ''
        ]]);
    }
}
