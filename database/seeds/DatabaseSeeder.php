<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(BukuTableSeeder::class);
         $this->call(BukuGenreTableSeeder::class);
         $this->call(DeviceElektronikSeeder::class);
         $this->call(DeviceElektronikTypeSeeder::class);
         $this->call(BackpackSeeder::class);
    }
}
