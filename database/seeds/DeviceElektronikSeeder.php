<?php

use Illuminate\Database\Seeder;

class DeviceElektronikSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('device_elektroniks')->insert([[
            'type_id' => 1,
            'brand' => 'Lenovo',
            'series' => 'ideapad',
            'year' => 2018
        ],[
            'type_id' => 2,
            'brand' => 'MacBook',
            'series' => 'MacBook Pro',
            'year' => 2018
        ],[
            'type_id' => 3,
            'brand' => 'Oppo',
            'series' => 'F1 Plus',
            'year' => 2016
        ],[
            'type_id' => 4,
            'brand' => 'Kenwood',
            'series' => 'Kenwood Monster',
            'year' => 2017
        ],[
            'type_id' => 3,
            'brand' => 'Samsung',
            'series' => 'Galaxy S9',
            'year' => 2018
        ],[
            'type_id' => 2,
            'brand' => 'HP',
            'series' => 'Pavilion',
            'year' => 2017
        ]]);
    }
}
