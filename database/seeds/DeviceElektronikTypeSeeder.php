<?php

use Illuminate\Database\Seeder;

class DeviceElektronikTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('device_elektronik_types')->insert([[
            'type' => 'PC'
        ],[
            'type' => 'Laptop'
        ],[
            'type' => 'Handphone'
        ],[
            'type' => 'Speaker'
        ]]);
    }
}
