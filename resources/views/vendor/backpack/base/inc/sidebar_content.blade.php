<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
<li><a href="{{ backpack_url('elfinder') }}"><i class="fa fa-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
<li><a href='{{ backpack_url('tag') }}'><i class='fa fa-tag'></i> <span>Tags</span></a></li>
<li class="treeview">
    <a href="#"><i class="fa fa-group"></i> <span>Users, Roles, Permissions</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
      <li><a href="{{ backpack_url('user') }}"><i class="fa fa-user"></i> <span>Users</span></a></li>
      <li><a href="{{ backpack_url('role') }}"><i class="fa fa-group"></i> <span>Roles</span></a></li>
      <li><a href="{{ backpack_url('permission') }}"><i class="fa fa-key"></i> <span>Permissions</span></a></li>
    </ul>
  </li>
<li class="treeview">
<a href="#"><i class="fa fa-book"></i> <span>Buku</span> <i class="fa fa-angle-left pull-right"></i></a>
  <ul class="treeview-menu">
    <li><a href='{{ backpack_url('buku') }}'><i class='fa fa-circle'></i> <span>Daftar Buku</span></a></li>
    <li><a href='{{ backpack_url('bukugenre') }}'><i class='fa fa-circle'></i> <span>Genre Buku</span></a></li>
  </ul>
</li>
<li class="treeview">
<a href="#"><i class="fa fa-laptop"></i> <span>Device</span> <i class="fa fa-angle-left pull-right"></i></a>
  <ul class="treeview-menu">
  <li><a href='{{ backpack_url('device') }}'><i class='fa fa-circle'></i> <span>Device Elektronik</span></a></li>
    <li><a href='{{ backpack_url('devicetype') }}'><i class='fa fa-circle'></i> <span>Device Type</span></a></li>
  </ul>
</li>

<li><a href='{{ backpack_url('peminjaman') }}'><i class='fa fa-shopping-bag'></i> <span>Peminjaman</span></a></li>