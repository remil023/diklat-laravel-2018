<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\DeviceRequest as StoreRequest;
use App\Http\Requests\DeviceRequest as UpdateRequest;

/**
 * Class DeviceCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class DeviceCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Device');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/device');
        $this->crud->setEntityNameStrings('device', 'devices');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();

        // add asterisk for fields that are required in DeviceRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        $this->crud->addField([   // Select2
            'label' => "Type",
            'type' => 'select',
            'name' => 'type_id', // the db column for the foreign key
            'entity' => 'Devicetypes', // the method that defines the relationship in your Model
            'attribute' => 'type', // foreign key attribute that is shown to user
            'model' => "App\Models\Devicetype" // foreign key model
        ]);

        $this->crud->addColumn([
            'label' => "Type",
            'type' => 'select',
            'name' => 'type_id', // the db column for the foreign key
            'entity' => 'Devicetypes', // the method that defines the relationship in your Model
            'attribute' => 'type', // foreign key attribute that is shown to user
            'model' => "App\Models\Devicetype" // foreign key model
        ]);

        $this->crud->addColumn([
            'label' => "#",
            'type' => 'row_number',
            'name' => 'row_number',
            'orderable' => false
        ])->makeFirstColumn();

        $this->crud->enableExportButtons();
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
