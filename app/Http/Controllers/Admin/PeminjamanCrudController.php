<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\PeminjamanRequest as StoreRequest;
use App\Http\Requests\PeminjamanRequest as UpdateRequest;

/**
 * Class PeminjamanCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class PeminjamanCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Peminjaman');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/peminjaman');
        $this->crud->setEntityNameStrings('peminjaman', 'peminjamen');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();

        // add asterisk for fields that are required in PeminjamanRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        $this->crud->addField([   // Select2
            'label' => "Peminjam",
            'type' => 'select2',
            'name' => 'user_id', // the db column for the foreign key
            'entity' => 'PeminjamanUser', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\User" // foreign key model
        ]);

        $this->crud->addField([   // Select2
            'label' => "Buku",
            'type' => 'select2',
            'name' => 'item_id', // the db column for the foreign key
            'entity' => 'PeminjamanBuku', // the method that defines the relationship in your Model
            'attribute' => 'judul', // foreign key attribute that is shown to user
            'model' => "App\Models\Buku" // foreign key model
        ]);

        $this->crud->removeField('time_returned'); // remove a column from the table
        $this->crud->removeField('time_updated'); // remove a column from the table

        $this->crud->addField([   // Date
            'label' => 'Tgl Pinjam',
            'name' => 'time_added',
            'type' => 'datetime_picker'
        ]);

        $this->crud->addColumn([
            'label' => "#",
            'type' => 'row_number',
            'name' => 'row_number',
            'orderable' => false
        ])->makeFirstColumn();

        $this->crud->addColumn([
            'label' => "Peminjam",
            'type' => 'select2',
            'name' => 'user_id', // the db column for the foreign key
            'entity' => 'PeminjamanUser', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\User" // foreign key model
        ]);

        $this->crud->addColumn([
            'label' => "Buku",
            'type' => 'select2',
            'name' => 'item_id', // the db column for the foreign key
            'entity' => 'PeminjamanBuku', // the method that defines the relationship in your Model
            'attribute' => 'judul', // foreign key attribute that is shown to user
            'model' => "App\Models\Buku" // foreign key model
        ]);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
